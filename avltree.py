"""
avltree.py by Lessandro Mariano
https://github.com/lessandro/avltree
"""


class AVLTree(object):
    """
    AVL tree - self-balancing binary search tree
    http://en.wikipedia.org/wiki/AVL_tree

    This is a rather space-inefficient implementation: at any given point there
    will be n+1 empty nodes (all leaves) in a tree with n inserted items. This
    design however provides us with the liberty to do pointer manipulations
    without having to worry about acessing invalid child references.

    Another benefit of that is being able to represent an empty tree as a
    single (empty) tree node, eliminating the need for separate classes for the
    tree and nodes.
    """

    # declaring a slots array yields a huge improvement on memory consumption
    __slots__ = ['_cmpfunc', '_data', '_height', '_child']

    def __init__(self, cmpfunc=cmp):
        # comparison function (detaults to the built-in cmp function)
        self._cmpfunc = cmpfunc
        # data attached to this node
        self._data = None
        # tree height
        self._height = 0
        # list of children
        self._child = []

    def insert(self, data):
        """
        Insert a new element in the tree and rebalance it if necessary.
        """
        if self.is_leaf:
            # leaf node, set the data and create empty children
            self._data = data
            self._height = 1
            self._child = [AVLTree(self._cmpfunc), AVLTree(self._cmpfunc)]
        else:
            # map the result of the comparison function to a side of the tree
            # 0 for left side (less than self.data)
            # 1 for right side (greater than or equal self.data)
            side = (self._cmp(data) + 2) // 2
            self._child[side].insert(data)
            self._update_height()
            self._rebalance()

    def _rebalance(self):
        """
        Rebalance the tree if it's unbalanced.

        Since half of the possible rotations for an AVL tree is a mirror of the
        other half, this function abstracts both cases in one using the
        variable `side` to (and `1 - side`) to refer to one child or the other.
        """
        if self.is_leaf or self._balance not in [-2, 2]:
            return

        # which side is bigger (0: left, 1: right)
        side = (self._balance + 2) // 4
        # check for left-right or right-left rotations
        if self._child[side]._balance == side * -2 + 1:
            # rotate child and subchild
            self._child[side]._rotate(1 - side)

        # rotate self and child
        self._rotate(side)

    def _rotate(self, side):
        """
        Rotate the tree replacing self with one of its children (as specified
        by `side`). The replace is done in place so that references to the
        root node will continue pointing to the root of the updated tree.
        """
        child = self._child[side]

        # swap data
        self._data, child._data = child._data, self._data

        # update child pointers
        sc, cc = self._child, child._child
        sc[side], sc[1 - side], cc[side], cc[1 - side] = (
            cc[side], child, cc[1 - side], sc[1 - side]
        )

        # update heights
        child._update_height()
        self._update_height()

    def _update_height(self):
        """
        Recalculate the height of this tree.
        """
        if self.is_leaf:
            self._height = 0
        else:
            self._height = max(self._child[0].height, self._child[1].height)
            self._height += 1

    def _cmp(self, other):
        """
        This function wraps around the user-supplied comparison function,
        normalizing its output to {-1, 0, 1}.
        """
        cmpval = self._cmpfunc(other, self._data)
        if cmpval < 0:
            return -1
        if cmpval > 0:
            return 1
        return 0

    def lookup(self, data):
        """
        Lookup an item in the tree.

        Returns a tuple with the data if found, otherwise None.
        """
        if self.is_leaf:
            return None

        cmpval = self._cmp(data)
        if cmpval == 0:
            return (self._data,)

        # recurse
        return self._child[(cmpval + 1) // 2].lookup(data)

    def remove(self, data):
        """
        Remove a node from the tree.

        Implemented in a recursive way because we don't keep references to the
        parent node.
        """
        if self.is_leaf:
            return False

        cmpval = self._cmp(data)
        if cmpval != 0:
            # recurse
            removed = self._child[(cmpval + 1) // 2].remove(data)
            if removed:
                # removing a child might unbalance the tree
                self._update_height()
                self._rebalance()
            return removed
        else:
            # found it, remove self
            self._remove_self()
            return True

    def _remove_self(self):
        if not self._child[0].is_leaf:
            # take the data of the largest (rightmost) node in the left subtree
            self._data = self._child[0]._take_rightmost()
            self._update_height()
            self._rebalance()
        else:
            # become your right child if there is no left subtree
            self._become_child(1)

    def _take_rightmost(self):
        """
        Take the data of the rightmost node in this tree, then destroy it.
        We destroy it by replacing the node with its left child. This works
        even if the child is an empty tree.
        """
        data = self._data

        if self._child[1].is_leaf:
            # replace self with left child
            self._become_child(0)
        else:
            # follow the tree until we find the largest value
            data = self._child[1]._take_rightmost()
            self._update_height()
            self._rebalance()

        return data

    def _become_child(self, side):
        """
        Replace self with one of its children. Used by the delete method.
        """
        self._data = self._child[side]._data
        self._height = self._child[side]._height
        self._child = self._child[side]._child

    # properties

    @property
    def _balance(self):
        return self._child[1].height - self._child[0].height

    @property
    def height(self):
        return self._height

    @property
    def is_leaf(self):
        """
        Leaf node iif child list is empty.
        """
        return not self._child

    # pretty print

    def __str__(self):
        if self.is_leaf:
            return ''

        return '(%s %s %s)' % (self._child[0], self._data, self._child[1])

    def pretty(self, depth=0):
        if self.is_leaf:
            return

        self._child[0].pretty(depth + 1)
        print '%s[%s] %s %s' % (
            '  ' * depth, self._data, self.height, self._balance
        )
        self._child[1].pretty(depth + 1)


def t1():
    t = AVLTree()
    for i in xrange(20):
        n = random.randint(0, 99)
        print 'inserting', n
        t.insert(n)
        t.pretty()
        raw_input()


def t2():
    t = AVLTree()
    gc.disable()
    t0 = time.time()
    for i in xrange(100000):
        t.insert(random.randint(0, 999999999))
        if i % 1000 == 0:
            t1 = time.time()
            print '%d\t%g' % (i, (t1 - t0))
            t0 = time.time()
    raw_input()
    gc.enable()


def t3():
    t = AVLTree()

    for i in xrange(31):
        t.insert(i)

    t.pretty()
    while True:
        t.remove(int(raw_input("remove: ")))
        t.pretty()


if __name__ == '__main__':
    import random
    import time
    import gc
    t3()
