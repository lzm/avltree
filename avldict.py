"""
avldict.py by Lessandro Mariano
https://github.com/lessandro/avltree
"""

from avltree import AVLTree


class AVLDict(object):
    """
    Dictionary class based on AVL trees
    """
    @classmethod
    def _cmpfunc(cls, a, b):
        return cmp(a[0], b[0])

    def __init__(self):
        self._tree = AVLTree(AVLDict._cmpfunc)

    def _lookup(self, key):
        result = self._tree.lookup([key, None])
        return result[0] if result else None

    def __setitem__(self, key, value):
        data = self._lookup(key)
        if data:
            data[1] = value
        else:
            self._tree.insert([key, value])

    def __getitem__(self, key):
        data = self._lookup(key)
        if not data:
            raise KeyError(key)
        return data[1]

    def get(self, key, default=None):
        data = self._lookup(key)
        if data:
            return data[1]
        else:
            return default

    def __contains__(self, item):
        return self._lookup(item) is not None

    def __delitem__(self, key):
        if not self._tree.remove([key, None]):
            raise KeyError(key)


def td():
    a = AVLDict()
    a["a"] = 4
    a["z"] = 5
    a["v"] = 1
    a["c"] = 4
    a["a"] = 4
    a["f"] = 5
    print a.get("a")
    print a.get("c")
    print a["a"]
    print a._tree
    del a["a"]
    print a.get("a")
    print a["a"]

if __name__ == '__main__':
    td()
